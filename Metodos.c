#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "utils.h"
#include "sislin.h"
#include "Metodos.h"

/*!
  \brief Método da Eliminação de Gauss

  \param SL Ponteiro para o sistema linear
  \param x ponteiro para o vetor solução

  \return código de erro. 0 em caso de sucesso.
*/
int eliminacaoGauss (SistLinear_t *SL, real_t *x, int pivot)
{
  transformaTriangular(SL,pivot);
  resolveTriangular(SL, x);
  return 0;
}

int resolveTriangular(SistLinear_t *SL, real_t *x)
{
  int tam = SL->n;
  for (int i = tam -1; i >= 0; --i)
  {
    x[i] = SL->b[i];
    for ( int j = i+1; j < tam; ++j)
    {
      x[i] -= SL->A[i][j] * x[j];
    }
    x[i] /= SL->A[i][i];
  }
}

int transformaTriangular(SistLinear_t *SL, int pivot)
{
    int tam = SL->n;
  for(int i = 0; i < tam; i++)
  {
    if (pivot)
      pivoteamentoParcial(SL, i);
    for (int k = i+1; k < tam; ++k)
    {
      real_t multiplicador = SL->A[k][i] / SL->A[i][i];
      SL->A[k][i] = 0.0;
      for(int j = i+1; j< tam; ++j)
      {
        SL->A[k][j] -= SL->A[i][j] * multiplicador;
      }
      SL->b[k] -= SL->b[i] * multiplicador;
    }
    
  }
  return 0;
}

int trocaLinha (SistLinear_t *SL, int linhaAtual, int linhaMaior)
{
  int tam = (SL ->n) -linhaAtual; 
  int coluna = linhaAtual;
  if (linhaAtual == linhaMaior)
    return -1;
  real_t *vetorAux;
  vetorAux = (real_t*) malloc( tam * sizeof(real_t));
  for(int i =0; i < tam; i++)
  {
    vetorAux[i] = SL->A[linhaAtual][coluna+i];
    SL->A[linhaAtual][coluna+i] = SL->A[linhaMaior][coluna+i];
    SL->A[linhaMaior][coluna+i] = vetorAux[i];
  }
  free(vetorAux);
  return 0;
}

int pivoteamentoParcial (SistLinear_t *SL, int linhaAtual )
{
  int tam = SL -> n;
  int coluna = linhaAtual;
  int linhaMaior = linhaAtual;
  for(int linha = linhaAtual+1; linha< tam ; linha++)
  {
    if (SL->A[linha][coluna] > SL->A[linhaMaior][coluna])
    {
      linhaMaior = linha;
    }
  }
  trocaLinha(SL, linhaAtual, linhaMaior);
  return 0;
}

/*!
  \brief Essa função calcula a norma L2 do resíduo de um sistema linear 

  \param SL Ponteiro para o sistema linear
  \param x Solução do sistema linear
*/
real_t normaL2Residuo(SistLinear_t *res)
{
  real_t norma = 0.0;
  int tam = res->n;

  for (int i = 0; i < tam; ++i)
    for (int j = 0; j < tam; ++j)
      norma += res->A[i][j]*res->A[i][j];
  norma = sqrt(norma);
  return norma;
}

/*!
  \brief Método de Refinamento

  \param SL Ponteiro para o sistema linear
  \param x ponteiro para o vetor solução
  \param erro menor erro aproximado para encerrar as iterações

  \return código de erro. Um nr positivo indica sucesso e o nr
          de iterações realizadas. Um nr. negativo indica um erro.
  */
int refinamento (SistLinear_t *SL, SistLinear_t *Inv, int k)
{
  int tam = SL->n, it;
  SistLinear_t *res = alocaSisLin(tam,0);
  real_t norma = 1.0;
  for (it = 0; it < k; ++it) {
    for (int i = 0; i < tam; ++i)
      for (int j = 0; j < tam; ++j)
        if (i == j)
          res->A[i][j] = 1.0;
        else
          res->A[i][j] = 0.0;
    for (int i = 0; i < tam; ++i)
      for (int j = 0; j < tam; ++j)
        res->A[i][j] -= produtoLinhaColuna(SL,Inv,i,j);
    norma = normaL2Residuo(res);
    printf("# iter %d: <||%.15g||>\n",it+1, norma);
    if (norma <= ERRO) {
      liberaSisLin(res);
      return it;
    }
    SistLinear_t *w = alocaSisLin(tam,0);
    for (int i = 0; i < tam; ++i) {
      for (int j = 0; j < tam; ++j)
        SL->b[j] = res->A[j][i];
      real_t *x = malloc(tam*sizeof(real_t));
      eliminacaoGauss(SL,x,0);
      for (int j = 0; j < tam; ++j)
        w->A[j][i] = x[j];
      free(x);
    }
    for (int i = 0; i < tam; ++i) 
      for (int j = 0; j < tam; ++j)
        Inv->A[i][j] += w->A[i][j];
    liberaSisLin(w);
  }
  liberaSisLin(res);
  return it;
}
/*
  retorna 1 para sucesso e -1 em falha
*/
int copiaSistema (SistLinear_t * SL, SistLinear_t *auxSL)
{
  if (SL->n != auxSL->n)
    return -1;
  for (int i = 0; i < SL->n; i++)
  {
    auxSL->b[i] = SL->b[i];
    for(int j = 0; j < SL->n; j++)
    {
      auxSL->A[i][j] = SL->A[i][j];
    }
  }
  return 1;
}

void prnTempos (double tempoLU, double tempoIter, double tempoResiduo) {
  printf("# Tempo LU: %.15g\n",tempoLU);
  printf("# Tempo iter: %.15g\n",tempoIter);
  printf("# Tempo residuo: %.15g\n#\n",tempoResiduo);
}

void liberaTudo (SistLinear_t *SL, SistLinear_t *matL, SistLinear_t *matU, SistLinear_t *Inv) {
  liberaSisLin(SL);
  liberaSisLin(matL);
  liberaSisLin(matU);
  liberaSisLin(Inv);
}

real_t produtoLinhaColuna (SistLinear_t *matA, SistLinear_t *matB, int lin, int col) {
  real_t produto = 0.0;
  int tam = matA->n;

  for (int i = 0; i < tam; ++i)
    produto += matA->A[lin][i]*matB->A[i][col];
  return produto;
}

 
/* função recursiva para encontrar o determinante*/
real_t determinante(SistLinear_t *matA, int n)
{    
  //  Caso base para matA ter 1 elemento
  if (n == 1)
    return matA->A[0][0];
  real_t D = 1; 
  int tam = matA->n;
  SistLinear_t* auxMatA = alocaSisLin(auxMatA->n, pontPont);
  copiaSistema(matA, auxMatA);

    

  transformaTriangular(auxMatA, 1);
  for(int i = 0; i < tam; i++)
  {
    D = D * auxMatA->A[i][i];
  }
    
  liberaSisLin(auxMatA);

  return D;
}

