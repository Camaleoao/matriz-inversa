#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"
#include "sislin.h"
#include "fatLU.h"
#include "Metodos.h"

int main(int argc, char const *argv[])
{
    int rand = 0, numMaxit, e = 0;
	if (argc == 1) {
		printf("%s [-e arquivo_entrada] [-s arquivo_saida] [-r N] -i k\n", argv[0]);
	}
    if(argc > 1)
    {
		SistLinear_t *matA, *matL, *matU, *Inv;
		int *trocas;
        int it = 1;
        double tempoLU, tempoIter, tempoResiduo;

        while(it < argc)
        {
            if(strcmp(argv[it], "-e") == 0)
            {   e = 1;
                FILE *arqEn;
                it++;

                arqEn = fopen(argv[it], "r");
                if(arqEn == NULL)
                    exit(1);

                freopen(argv[it], "r", stdin);
                it++;
                
            }
            if(strcmp(argv[it], "-s") == 0)
            {
                it++;
        		
				FILE *arqSa; 
                arqSa = fopen(argv[it], "w+");


                freopen(argv[it], "w", stdout);
                it++;
            }
            if(strcmp(argv[it], "-r") == 0)
            {
                it++;
                rand = strtol(argv[it], NULL, 10);
                it++;
            }
            if(strcmp(argv[it], "-i") == 0)
            {
                it++;
                numMaxit = strtol(argv[it], NULL, 10);
                it++;
            }
        }        
        if (rand) {
			matA = alocaSisLin(rand,0);
			if (!matA) {
				fprintf(stderr,"Falha na alocação de memória.\n");
				exit(-1);
			}
			srand(202201);
			iniSisLin(matA,0,1024.0);
		}
		else{
		
			matA = lerSisLin(stdin,0);

		}
		matL = alocaSisLin(matA->n,0);
		matU = alocaSisLin(matA->n,0);
		Inv = alocaSisLin(matA->n,0);
		trocas = malloc(matA->n*sizeof(int));
		if (!matL || !matU || !Inv || !trocas) {
			fprintf(stderr,"Falha na alocação de memória.\n");
			exit(-1);
		}
		printf("CONSEGUI ENTRAR NA DETERMINANTE\n");
		if (abs(determinante(matA, matA->n)) < 0.0000000000000000000000000001) {
			fprintf(stderr,"Matriz nao inversivel.\n");
			exit(-1);
		}
		printf("CONSEGUI SAIR DA DETERMINANTE\n");
		inicializaVetorTrocas(trocas,matA->n);
		tempoLU = timestamp();
		encontraLU(matA,matL,matU,trocas,&tempoLU);
		tempoIter = timestamp();
		encontraInversa(matL,matU,Inv,trocas);
        

		
		tempoResiduo = timestamp();
		
		refinamento(matA,Inv,numMaxit);
		tempoResiduo = timestamp() - tempoResiduo;
		tempoIter = timestamp() - tempoIter;
		
		//saidas 
		prnTempos(tempoLU,tempoIter,tempoResiduo);
		printf("%d\n",matA->n);
		prnMatriz(Inv);
		liberaTudo(matA,matL,matU,Inv);
		free(trocas);
    }
    return 0;
}