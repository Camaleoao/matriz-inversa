#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "utils.h"
#include "sislin.h"
#include "Metodos.h"
#include "fatLU.h"

/*  Retorna tempo em milisegundos

    Forma de uso:
 
    double tempo;
    tempo = timestamp();
    <trecho de programa do qual se deseja medir tempo>
    tempo = timestamp() - tempo;
*/

//retorna 0 se funciona
int encontraLU(SistLinear_t *MatrizOriginal, SistLinear_t *MatrizL, SistLinear_t *MatrizU, int* V,
              double *tempo)
{
    *tempo = timestamp();
    copiaMatriz(MatrizOriginal, MatrizU);
    int linha1;
    int linha2;
    int houvetroca;

    //triangularizacao de U e multiplicadores em L 
    for(int i = 0; i< MatrizOriginal->n; i++)
    {
        pivoteamentoParcialLU(MatrizU, i, V, &houvetroca, &linha1, &linha2);
        
        if (i > 0 && houvetroca)
        {
          trocaLinhas(MatrizL, linha1, linha2);
        }
        
        for(int k = i+1; k< MatrizOriginal->n; ++k)
        {
            real_t multiplicador = MatrizU->A[k][i] / MatrizU->A [i][i];
            MatrizU->A[k][i] = 0.0;
            MatrizL->A[k][i] = multiplicador;
            for(int j = i+1; j<MatrizOriginal->n; ++j)
            {
                MatrizU->A[k][j] -= MatrizU->A[i][j] * multiplicador; 
            }
        }
    }
    
    //diagonal de L e zera a parte superior
    for (int i = 0; i < MatrizOriginal->n; i++)
    {
        for (int j = i; j<MatrizOriginal->n; j++)
        {
            if (i == j)
                MatrizL->A[i][j] = 1.0;
            else
                MatrizL->A[i][j] = 0.0;

        }
    }
  
    vetorTransposto(V, MatrizOriginal->n);

    *tempo = timestamp() - *tempo;

    if (*tempo <=0)
    {
    	fprintf(stderr,"tempo de LU não contabilizado.\n");
      return -1;
    }

    return 0;
    
}

void vetorTransposto(int *V, int n)
{
  int *vetorAux;
  vetorAux = (int*) malloc (n * sizeof(int));
   
  for (int i = 0; i < n; i++)
  {
    vetorAux[V[i]] = i; 
  }
  
  for (int i = 0; i<n; i++)
  {
    V[i] = vetorAux[i];
  }

  free(vetorAux);
}

void inicializaVetorTrocas(int *V, int n)
{
    for(int i = 0; i < n; i++)
    {
        V[i] = i;
    }
    return;
}

int copiaMatriz (SistLinear_t * SL1, SistLinear_t *SL2)
{
  if (SL1->n != SL2->n)
    return -1;
  for (int i = 0; i < SL1->n; i++)
  {
    SL2->b[i] = SL1->b[i];
    for(int j = 0; j < SL1->n; j++)
    {
      SL2->A[i][j] = SL1->A[i][j];
    }
  }

  return 0;
}

int pivoteamentoParcialLU (SistLinear_t *mU, int linhaAtual, int *V, int *houvetroca, int *linha1, int *linha2)
{
  int tam = mU -> n;
  int coluna = linhaAtual;
  int linhaMaior = linhaAtual;
  for(int linha = linhaAtual+1; linha< tam ; linha++)
  {
    if (mU->A[linha][coluna] > mU->A[linhaMaior][coluna])
    {
      linhaMaior = linha;
    }
  }
  if (linhaAtual == linhaMaior)
  {
    *houvetroca = 0;
    *linha1 = -1;
    *linha2 = -1;
  }
  else
  {
    *houvetroca = 1;
    *linha1 = linhaAtual;
    *linha2 = linhaMaior;
  }
  trocaLinhas(mU, linhaAtual, linhaMaior);
  manutencaoVetorTrocas(V, linhaAtual, linhaMaior);
  return 0;
}

int trocaLinhas (SistLinear_t *SL, int linha1, int linha2)
{

  int tam = (SL ->n); 
  int coluna = linha1;
  if (linha1 == linha2)
  {

    return -1;
  }
  real_t *vetorAux;
  vetorAux = (real_t*) malloc( tam * sizeof(real_t));

  for(int i =0; i < tam; i++)
  {

    vetorAux[i] = SL->A[linha1][i];
    SL->A[linha1][i] = SL->A[linha2][i];
    SL->A[linha2][i] = vetorAux[i];
  }

  free(vetorAux);
  return 0;
}

int manutencaoVetorTrocas(int *V, int linhaAtual, int linhaMaior)
{
  int aux;
  aux = V[linhaAtual];
  V[linhaAtual] = V[linhaMaior];
  V[linhaMaior] = aux;
  return 0;
}

void encontraInversa(SistLinear_t *mL, SistLinear_t *mU, SistLinear_t *mInv, int* vetorTrocas)
{
  real_t *vetorSolucao;
  vetorSolucao = (real_t*) malloc( mL->n * sizeof(real_t));

  for(int i = 0; i < mL ->n; i++)
  {
    colunaIdentidade(mL, i, vetorTrocas);
    resolveTriangularInferior(mL, vetorSolucao);
    transformaSolucaoEmB(mU, vetorSolucao);
    resolveTriangularSuperior(mU, vetorSolucao);
    atribuiColunaInversa(vetorSolucao, mInv, i);
  }
  //ajustaPosLinhas(mInv, vetorTrocas);
  free(vetorSolucao);
}

void ajustaPosLinhas(SistLinear_t *mInv, int* V)
{
  int aux;

  for (int i = 0; i < mInv->n-1; i++)
  {
    for (int j = i; j < mInv->n; j++)
    {
      if (V[j] == i)
      {
        trocaLinhas(mInv, i, j);
        aux = V[i];
        V[i] = V[j];
        V[j] = aux;
      }
    }
  }
  
  return;
}

void atribuiColunaInversa(real_t *V, SistLinear_t *mInv, int coluna)
{
  for (int i = 0; i < mInv->n; i++)
  {
    mInv->A[i][coluna] = V[i];
  }
  
  return;
}

void transformaSolucaoEmB(SistLinear_t *mU, real_t *x)
{
  for (int i = 0; i<mU->n; i++)
  {
    mU->b[i] = x[i];
  }
  return;
}

int resolveTriangularSuperior(SistLinear_t *SL, real_t *x)
{
  int tam = SL->n;
  for (int i = tam -1; i >= 0; --i)
  {
    x[i] = SL->b[i];
    for ( int j = i+1; j < tam; ++j)
    {
      x[i] -= SL->A[i][j] * x[j];
    }
    
    if (SL->A[i][i] == 0)
    {
      fprintf(stderr,"Erro :divisao por zero.\n");
      return -1;
    }
    x[i] /= SL->A[i][i];
  }
  return 0;
}

int resolveTriangularInferior(SistLinear_t *SL, real_t *x)
{
  int tam = SL->n;
  for (int i = 0; i < tam; ++i)
  {
    x[i] = SL->b[i];
    for ( int j = 0; j < i; ++j)
    {
      x[i] -= SL->A[i][j] * x[j];
    }
    
    if (SL->A[i][i] == 0)
    {
      fprintf(stderr,"Erro :divisao por zero.\n");
      return -1;
    }

    x[i] /= SL->A[i][i];
  }
  return 0;
}

void colunaIdentidade(SistLinear_t *mL, int i, int* V)
{
  int aux;

  aux = V[i];
  for (int j = 0; j < aux; j++)
  {
    mL->b[j] = 0.0;
  }
  mL->b[aux] = 1.0;

  for (int j = aux+1; j < mL->n; j++)
  {
    mL->b[j] = 0.0;
  }  

}
