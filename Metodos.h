#ifndef __METODOS_H__
#define __METODOS_H__

// Parâmetros para teste de convergência
#define MAXIT 100     // número máximo de iterações para métodos iterativos
#define ERRO 1.0e-6   // Tolerância para critérios de parada em métodos iterativos

// Calcula a normaL2 do resíduo
real_t normaL2Residuo(SistLinear_t *res);

// Método da Eliminação de Gauss
int eliminacaoGauss (SistLinear_t *SL, real_t *x, int pivot);
int resolveTriangular(SistLinear_t *SL, real_t *x);
int transformaTriangular(SistLinear_t *SL, int pivot);
int pivoteamentoParcial (SistLinear_t *SL, int linhaAtual);


// Método de Refinamento
int refinamento (SistLinear_t *SL, SistLinear_t *Inv, int k);
int copiaSistema (SistLinear_t * SL, SistLinear_t *auxSL);


int trocaLinha (SistLinear_t *SL, int linhaAtual, int linhaMaior);
#endif // __METODOS_H__

int resolveTriangular(SistLinear_t *SL, real_t *x);

void prnTempos (double tempoLU, double tempoIter, double tempoResiduo);

void liberaTudo (SistLinear_t *SL, SistLinear_t *matL, SistLinear_t *matU, SistLinear_t *Inv);

real_t produtoLinhaColuna (SistLinear_t *matA, SistLinear_t *matB, int lin, int col);

void Cofator(SistLinear_t *matA, SistLinear_t *temp, int p, int q, int n);
real_t determinante(SistLinear_t *matA, int n);