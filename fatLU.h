//metodos para encontrar LU

int encontraLU(SistLinear_t *MatrizOriginal, SistLinear_t *MatrizL, SistLinear_t *MatrizU, int* V, double *tempo);
void inicializaVetorTrocas(int *V, int n);
int copiaMatriz (SistLinear_t * SL1, SistLinear_t *SL2);
int pivoteamentoParcialLU (SistLinear_t *mU, int linhaAtual, int *V, int *houvetroca, int *linha1, int *linha2);
int trocaLinhas (SistLinear_t *SL, int linhaAtual, int linhaMaior);
int manutencaoVetorTrocas(int *V, int linhaAtual, int linhaMaior);
void vetorTransposto(int *V, int n);

//metodos para encontrar inversa
void encontraInversa(SistLinear_t *mL, SistLinear_t *mU, SistLinear_t *mInv, int* vetorTrocas);
void ajustaPosLinhas(SistLinear_t *mInv, int* V);
void atribuiColunaInversa(real_t *V, SistLinear_t *mInv, int coluna);
void transformaSolucaoEmB(SistLinear_t *mU, real_t *x);
int resolveTriangularSuperior(SistLinear_t *SL, real_t *x);
int resolveTriangularInferior(SistLinear_t *SL, real_t *x);
void colunaIdentidade(SistLinear_t *mL, int i, int* V);
